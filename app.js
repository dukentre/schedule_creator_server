/**
 * Входной файл сервера
 * @file
 */

//============[инициализируем модули]=================
const express = require('express');
const db = require("./own_modules/db");

//============[Экземпляр экспресс]=================
const app = express();


//============[подключаем хистори мод, так как у нас spa]=================
var history = require('connect-history-api-fallback');
app.use(history());


//======[Инициализируем сессию]============
let session = require('express-session');
let MySQLStore = require('express-mysql-session')(session);
let sessionStore = new MySQLStore(db.mysqlSettings);
let sessionMiddleware = session({  // Session
    secret: 'dukentreBilZdes',
    saveUninitialized: true,
    resave: true,
    name: 'sSesSionId',
    store: sessionStore,

});
app.use(sessionMiddleware);

//============[подключаем статические файлы]=================
app.use(express.static(__dirname + '/public_html')); //нужно для доступа к файлам на локалке. создай папку "public_html" рядом со скриптом и закинь туда файлы из папки public_html, которая находится на сервере.

//============[запускаем сервер]=================
let server = require('http').createServer(app);

//============[инициализируем сокеты]=================
let io = require("socket.io")(server);

//======[Расшариваем сессию между socket.io и express]============
const sharedsession = require('express-socket.io-session');
io.use(sharedsession(sessionMiddleware));

//============[запускаем наши модули (сокеты и апи)]=================
const socket = require("./own_modules/sockets/mainSocket")(io);//тут сокеты
const api = require("./own_modules/server_api/mainApi")(app);//тут роутинг по апи

//============[начинаем прослушивать сервер]=================
server.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});