/**
 * Функция генерации расписания
 * @param {number} weekId - id недели(нужно для генерации ссылки)
 * @param {number} semesterId - id семестра(нужно для генерации ссылки)
 * @param {object} settingsJson - Json с настройками сохранения
 * @param {string} scheduleJson - Json с расписанием на неделю
 * @function
 * @return {string} Ссылка на скачивание excel файла расписания
 */
function GenerateScheduleWithSettings(weekId,semesterId,settingsJson,scheduleJson) {
    console.log(weekId,semesterId,settingsJson);
    const xlsx = require("excel4node");
    //const fs = require("fs");

    //let jsonFile = fs.readFileSync(__dirname + "/schedule.json");//подгружаем json


    let groups = JSON.parse(scheduleJson); //создаём объект с расписанием для групп

    let sheets = {};//объект с листами
    let index = 0;
    let sheet = 0;
    let days = {};
    let daysInWeek = 0;

    function generateDays(daysInWeek){
        let answerDays = {
            1: {name: 'Понедельник', groups: {}, length: 0},
            2: {name: 'Вторник', groups: {}, length: 0},
            3: {name: 'Среда', groups: {}, length: 0},
            4: {name: 'Четверг', groups: {}, length: 0},
            5: {name: 'Пятница', groups: {}, length: 0},
        }
        if(daysInWeek>5){
            answerDays[6] = {name: 'Суббота', groups: {}, length: 0};
        }
        if(daysInWeek>6){
            answerDays[7] = {name: 'Воскресенье', groups: {}, length: 0};
        }
        return answerDays;
    }
    let settings = JSON.parse(settingsJson);
    for(let sheetOf of settings){
        for(let groupName of sheetOf.groups){
            console.log(groupName);
            let group = groups[groupName];
            if(daysInWeek === 0){
                daysInWeek = Object.keys(group).length-1;
                days = generateDays(daysInWeek);
            }
                Object.keys(group).map(dayName => {//проходимся по группе
                    let lessons = group[dayName];

                    let length = Object.keys(lessons).length;//кол-во пар в дне

                    if (days[dayName] === undefined) {//если у нас оказался какой-то левый день
                        days[dayName] = {
                            name: 'unknow',
                            groups: {},
                            length: 0
                        };
                    }

                    days[dayName]['date'] = lessons['date'];//записываем в день дату
                    days[dayName]['length'] = Math.max(days[dayName]['length'], length);//устанавливаем кол-во пар в дне
                    days[dayName]['groups'][groupName] = lessons;//добавляем уроки группы в определённый день
                });

        }
        if (sheets[sheet] === undefined) {//если листа нет
            sheets[sheet] = {};//создаём его
        }
        sheets[sheet] = {days,title:sheetOf.title};//вставляем дни в лист
        sheet++;//переходим на следующий
        days = generateDays(daysInWeek);//обнуляем дни
    }

    /*Object.keys(groups).sort().map((groupName, groupIndex) => {//сортируем по имени и проходимся по массиву
        let group = groups[groupName];
        if(daysInWeek === 0){
            daysInWeek = Object.keys(group).length-1;
            days = generateDays(daysInWeek);
        }
        Object.keys(group).map(dayName => {//проходимся по группе
            let lessons = group[dayName];

            let length = Object.keys(lessons).length;//кол-во пар в дне

            if (days[dayName] === undefined) {//если у нас оказался какой-то левый день
                days[dayName] = {
                    name: 'unknow',
                    groups: {},
                    length: 0
                };
            }

            days[dayName]['date'] = lessons['date'];//записываем в день дату
            days[dayName]['length'] = Math.max(days[dayName]['length'], length);//устанавливаем кол-во пар в дне
            days[dayName]['groups'][groupName] = lessons;//добавляем уроки группы в определённый день
        });
        if ((groupIndex + 1) % 8 === 0 || groupIndex >= Object.keys(groups).length - 1) {//если кол-во групп кратно 8 или index новой группы последний
            if (sheets[sheet] === undefined) {//если листа нет
                sheets[sheet] = {};//создаём его
            }
            sheets[sheet] = days;//вставляем дни в лист
            sheet++;//переходим на следующий
            days = generateDays(daysInWeek);//обнуляем дни
        }
    });*/
//console.log(days);

    let wb = new xlsx.Workbook();//создаём новый документ

    const verticalText = wb.createStyle({alignment: {textRotation: 90}});

    const cell = wb.createStyle({alignment: {vertical: 'center'}});

    const textLeft = wb.createStyle({alignment: {horizontal: 'left'}});
    const textCenter = wb.createStyle({alignment: {horizontal: 'center'}});
    const textRight = wb.createStyle({alignment: {horizontal: 'right'}});
    const wrapText = wb.createStyle({alignment: {wrapText: true}});

    const borderTopNormal = wb.createStyle({border: {top: {style: 'thin', color: '#000000'}}});
    const borderBottomNormal = wb.createStyle({border: {bottom: {style: 'thin', color: '#000000'}}});
    const borderLeftNormal = wb.createStyle({border: {left: {style: 'thin', color: '#000000'}}});
    const borderRightNormal = wb.createStyle({border: {right: {style: 'thin', color: '#000000'}}});

    const borderTopBold = wb.createStyle({border: {top: {style: 'medium', color: '#000000'}}});
    const borderBottomBold = wb.createStyle({border: {bottom: {style: 'medium', color: '#000000'}}});
    const borderLeftBold = wb.createStyle({border: {left: {style: 'medium', color: '#000000'}}});
    const borderRightBold = wb.createStyle({border: {right: {style: 'medium', color: '#000000'}}});

    let teachers = {};
    let rooms = {};

    Object.keys(sheets).map((daysName) => {//проходимся по листам
        let days = sheets[daysName].days;//получаем дни
        let groupNames = Object.keys(days[Object.keys(days)[0]]['groups']);//получаем группы
        let sheetName = groupNames[0] + " - " + groupNames[groupNames.length - 1];//генерируем название комнаты
        let ws = wb.addWorksheet(sheetName);//добавляем в файл новый лист

        //Вставляем в ячейки утверждение
        ws.cell(1, 27).string("Утверждаю").style(cell)
            .style(textRight);
        ws.cell(2, 27).string("Директор ОБПОУ \"КГПК\"")
            .style(cell)
            .style(textRight);
        ws.cell(3, 27).string("_________ О.И.Морозова")
            .style(cell)
            .style(textRight);


        ws.cell(5, 1, 5, 27, true).string("Расписание замены занятий с 14.10.2019 г. по 18.10.2019 г.")
            .style(cell)
            .style(textCenter);
        ws.cell(6, 1, 6, 27, true).string(sheets[daysName].title ? sheets[daysName].title : '')//Отеделние
            .style(cell)
            .style(textCenter);

        ws.column(1).setWidth(3);// устанавливаем колонкам ширину
        ws.column(2).setWidth(3);
        ws.column(3).setWidth(6);
        ws.cell(8, 1, 9, 1, true).string("День")
            .style(cell)
            .style(textCenter).style(verticalText).style(wrapText)
            .style(borderLeftBold).style(borderTopBold).style(borderBottomBold);
        ws.cell(8, 2, 9, 2, true).string("Дата")
            .style(cell)
            .style(textCenter).style(verticalText).style(wrapText)
            .style(borderTopBold).style(borderBottomBold).style(borderLeftNormal);
        ws.cell(8, 3, 9, 3, true).string("№ урока")
            .style(cell)
            .style(textCenter).style(wrapText)
            .style(borderLeftBold).style(borderTopBold).style(borderBottomBold);

        let rowTopMargin = 0;
        let leftMargin = 0;
        Object.keys(days).map((dayName, dayIndex) => {//проходимся по дням
            let columnLeftMargin = 0;

            let day = days[dayName];
            let groups = day['groups'];

            ws.cell(10 + rowTopMargin, 1, 10 + rowTopMargin + day['length'] - 1, 1, true).string(day['name'])//вставляем название дня
                .style(cell)
                .style(textCenter).style(verticalText).style(wrapText)
                .style(borderLeftBold).style(borderTopBold).style(borderBottomBold);
            ws.cell(10 + rowTopMargin, 2, 10 + rowTopMargin + day['length'] - 1, 2, true).string(day['date'])//вставляем дату
                .style(cell)
                .style(textCenter).style(verticalText).style(wrapText)
                .style(borderTopBold).style(borderBottomBold).style(borderLeftNormal);
            for (let i = 0; i < day['length']; i++) {//вставляем номера пар
                ws.cell(10 + rowTopMargin + i, 3).string((i + 1) + " пара")
                    .style(cell)
                    .style(textCenter).style(wrapText)
                    .style(borderLeftBold).style(borderRightBold);
            }
            ws.cell(10 + rowTopMargin, 3)
                .style(borderTopBold);
            ws.cell(10 + rowTopMargin + day['length'] - 1, 3)
                .style(borderBottomBold);

            Object.keys(groups).map(groupName => {//проходимся по группам
                let lessons = groups[groupName];

                if (dayIndex === 0) {//строим шапку
                    ws.cell(8, 4 + columnLeftMargin, 8, 6 + columnLeftMargin, true).string(groupName)
                        .style(cell)
                        .style(textCenter).style(wrapText)
                        .style(borderTopBold).style(borderLeftBold).style(borderRightBold).style(borderBottomNormal);
                    ws.cell(9, 4 + columnLeftMargin).string("Дисциплина/МДК")
                        .style(cell)
                        .style(textCenter).style(wrapText)
                        .style(borderBottomBold).style(borderLeftBold);
                    ws.cell(9, 5 + columnLeftMargin).string("Преподаватель")
                        .style(cell)
                        .style(textCenter).style(wrapText)
                        .style(borderBottomBold).style(borderLeftNormal).style(borderRightNormal);
                    ws.cell(9, 6 + columnLeftMargin).string("каб.")
                        .style(cell)
                        .style(textCenter).style(wrapText)
                        .style(borderBottomBold).style(borderRightBold);
                }

                Object.keys(lessons).map((lessonName, index) => {//летип по парам
                    let pair = lessons[lessonName];

                    let pairName = "";
                    let pairTeacher = "";
                    let pairRoomNumber = "";

                    try {
                        pairName = pair['lesson'].toString();
                    } catch (e) {
                    }
                    try {
                        pairTeacher = pair['lessonTeacher'].toString();
                    } catch (e) {
                    }
                    try {
                        pairRoomNumber = pair['lessonRoomNumber'].toString();
                    } catch (e) {
                    }

                    try {
                        pairTeacher.trim().replace(/ +/gui, " ").match(/[^ ].+?.\..\./gui).map((teacherName, teacherIndex) => {//парсит учителей
                            if (teachers[teacherName] === undefined) {
                                teachers[teacherName] = 0;
                            }
                            teachers[teacherName]++;
                        });
                    } catch (e) {
                    }

                    pairRoomNumber.trim().replace(/ +/gui, " ").split(/ /).map((roomName, roomIndex) => {//парсит кабинеты
                        if (rooms[roomName] === undefined) {
                            rooms[roomName] = 0;
                        }
                        rooms[roomName]++;
                    });

                    ws.column(4 + columnLeftMargin).setWidth(30);
                    ws.column(5 + columnLeftMargin).setWidth(15);
                    ws.column(6 + columnLeftMargin).setWidth(5);

                    ws.cell(10 + rowTopMargin + index, 4 + columnLeftMargin).string(pairName)
                        .style(cell)
                        .style(textLeft).style(wrapText)
                        .style(borderLeftBold).style(borderRightNormal).style(borderBottomNormal);
                    ws.cell(10 + rowTopMargin + index, 5 + columnLeftMargin).string(pairTeacher)
                        .style(cell)
                        .style(wrapText)
                        .style(textLeft).style(borderRightNormal).style(borderBottomNormal);
                    ws.cell(10 + rowTopMargin + index, 6 + columnLeftMargin).string(pairRoomNumber)
                        .style(cell)
                        .style(textCenter).style(wrapText)
                        .style(borderRightBold).style(borderBottomNormal);
                });
                ws.cell(10 + rowTopMargin + day['length'] - 1, 4 + columnLeftMargin, 10 + rowTopMargin + day['length'] - 1, 6 + columnLeftMargin)
                    .style(cell)
                    .style(borderBottomBold);
                columnLeftMargin += 3;
            });
            rowTopMargin += day['length'];
            leftMargin = columnLeftMargin;
        });

        ws.cell(rowTopMargin + 13, 3).string("Зав.уч                                                                  О.Н. Шафоростова")//принтим того, кто составлял
            .style(cell)
            .style(textLeft);
    });


    wb.write(  `./public_html/files/excelFiles/schedule_semester${semesterId}_week${weekId}.xlsx`);
    return `/files/excelFiles/schedule_semester${semesterId}_week${weekId}.xlsx`;
}

module.exports = {
    GenerateScheduleWithSettings,
};