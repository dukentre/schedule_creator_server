/**
 * Модуль для обработки события выхода из аккаунта
 * @return {object}
 * @module authLogout
 *
 */

const helper = require("./../../helper");
const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);

        delete socket.handshake.session.userId;
        delete socket.handshake.session.connect;
        delete socket.handshake.session.email;
        socket.handshake.session.save();

        return resCreator.resolveAnswer();
    });
};

