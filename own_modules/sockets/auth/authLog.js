/**
 * Модуль для обработки события авторизации пользователя
 * @param {string} login - Логин пользователя
 * @param {string} password - Пароль пользователя
 * @return {object} Информация о пользователе
 * @module
 *
 */

const helper = require("./../../helper");
const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);

        if (helper.isExist(data, ['email', 'pass'])) {
            let sql = "SELECT id, email FROM accounts WHERE email = ? AND pass = md5(?)";
            let param = [data['email'], data['pass']];
            db.query(sql, param, (err, res) => {
                if(err){
                    return  resCreator.resolveErrorAnswer(err.code);
                }
                if(res[0]){
                    socket.handshake.session.userId = res[0]['id'];
                    socket.handshake.session.connect = true;
                    socket.handshake.session.email = data['email'];
                    socket.handshake.session.save();
                    return resCreator.resolveAnswer({email: data['email']})
                }else {
                    return  resCreator.resolveErrorAnswer("USER_NOT_FOUND")
                }
            })
        } else {
            if (socket.handshake.session.connect) {
                return resCreator.resolveAnswer({email: socket.handshake.session.email})
            }else{
                return  resCreator.resolveErrorAnswer("FIELDS_ERROR");
            }

        }
    });
};

