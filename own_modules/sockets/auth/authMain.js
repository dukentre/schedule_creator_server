/**
 * Регистратор событий связанных с авторизацией
 * @module
 * @param {registrator} registrator - Экземпляр регистратора
 */
module.exports = (registrator) =>{
    registrator.registAction('auth-log',require('./authLog')); // событие авторизации
    registrator.registAction('auth-logout',require('./authLogout'),{checkSession:true}); // событие выхода из аккаунта
};