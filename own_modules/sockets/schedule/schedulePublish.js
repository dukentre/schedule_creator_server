/**
 * Модуль для публикации расписания
 * @param {number} weekId
 * @return {object}
 * @module schedulePublish
 *
 */

let sqlWeekStartEnd = "SELECT `start`,`end` FROM `weeks` WHERE id = ?";
let sqlSchedule = `SELECT \`day\`, \`lessonNumber\`, \`id_group\`, (SELECT name FROM settings_groups WHERE id = schedule.id_group) as groupName,
        CONCAT( (SELECT name FROM settings_items WHERE id = \`id_item_p1\` ), ' ',
IF(\`type\` = 1 OR \`type\` = 3,'пр1 ',''), IF(\`type\` = 2,'пр2 ',''), 
        IF(\`id_item_p2\` IS NOT NULL,
CONCAT( (SELECT name FROM settings_items WHERE id = \`id_item_p2\` ), ' ', 
IF(\`type\` = 2 OR \`type\` = 3,'пр2 ','') ) 
,'' ) ) AS lesson,
        CONCAT( (SELECT name FROM settings_rooms WHERE id = \`id_room_p1\` ), ' ', 
        IF(\`id_room_p2\` IS NOT NULL, (SELECT name FROM settings_rooms WHERE id = \`id_room_p2\` ), '') ) AS room, 
        
        CONCAT( 
IF(\`type\` = 2,
   IF(
       (SELECT id_teacher_two FROM settings_items WHERE id = \`id_item_p1\`) IS NOT NULL,
       (SELECT CONCAT(last_name,' ',SUBSTRING(first_name,1,1),'.',SUBSTRING(father_name,1,1),'.') FROM settings_teachers WHERE \`id\` = (SELECT id_teacher_two FROM settings_items WHERE id = \`id_item_p1\` ) ),
       (SELECT CONCAT(last_name,' ',SUBSTRING(first_name,1,1),'.',SUBSTRING(father_name,1,1),'.') FROM settings_teachers WHERE \`id\` = (SELECT id_teacher_one FROM settings_items WHERE id = \`id_item_p1\` ) )
       ),
   (SELECT CONCAT(last_name,' ',SUBSTRING(first_name,1,1),'.',SUBSTRING(father_name,1,1),'.') FROM settings_teachers WHERE \`id\` = (SELECT id_teacher_one FROM settings_items WHERE id = \`id_item_p1\` ) )
   ),
        ' ',
        IF(\`type\` = 4,
        IF((SELECT id_teacher_two FROM settings_items WHERE id = \`id_item_p1\` ) IS NOT NULL,
        (SELECT CONCAT(last_name,' ',SUBSTRING(first_name,1,1),'.',SUBSTRING(father_name,1,1),'.') FROM settings_teachers WHERE \`id\` = (SELECT id_teacher_two FROM settings_items WHERE id = \`id_item_p1\` )), '')
,''),
        
        If(\`id_item_p2\` IS NOT NULL,CONCAT(
        (SELECT CONCAT(last_name,' ',SUBSTRING(first_name,1,1),'.',SUBSTRING(father_name,1,1),'.') FROM settings_teachers WHERE \`id\` = (SELECT id_teacher_one FROM settings_items WHERE id = \`id_item_p2\` ) ),
        ' ',
        
        IF((SELECT id_teacher_two FROM settings_items WHERE id = \`id_item_p2\` ) IS NOT NULL,
        (SELECT CONCAT(last_name,' ',SUBSTRING(first_name,1,1),'.',SUBSTRING(father_name,1,1),'.') FROM settings_teachers WHERE \`id\` = (SELECT id_teacher_two FROM settings_items WHERE id = \`id_item_p2\` )), '')
        ),'')
        ) AS teacher
        
        FROM \`schedule\` WHERE id_week = ?`;


const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let param = [data['weekId']];

        let weekStartDate = null;
        new Promise((resolveLocal, rejectLocal) => {
            db.query(sqlWeekStartEnd, param, (err, res) => {
                if (err) {
                    console.log("Schedule Publish [1]",err);
                    return rejectLocal(resCreator.resolveErrorAnswer(err.code));
                } else {
                    weekStartDate = new Date(res[0].start);
                    let first = new Date(res[0].start);
                    let second = new Date(res[0].end);
                    let daysInWeek = new Date(second - first).getDate();//кол-во дней в неделе
                    resolveLocal(daysInWeek);
                }
            });
        }).then((daysInWeek) => {
            db.query(sqlSchedule, param, (err, res) => {
                if (err) {
                    console.log("Schedule Publish [2]",err);
                    return resCreator.resolveErrorAnswer(err.code);
                } else {
                    let json = generateScheduleJson(res,weekStartDate,daysInWeek);

                    (async function(){
                        await new Promise((resolveLocal,rejectLocal)=>{
                            let sql = "DELETE FROM `schedule_json` WHERE `id_week` = ?"
                            let param = [data['weekId']];
                            db.query(sql,param,(err,res)=>{
                                if(err){
                                    console.log("Schedule Publish [3]",err);
                                    return rejectLocal(resCreator.resolveErrorAnswer(err.code));
                                }
                                return resolveLocal();
                            });
                        });
                        await new Promise((resolveLocal,rejectLocal)=>{
                            let sql = "INSERT INTO `schedule_json`(`id_week`, `json`) VALUES (?,?)";
                            let param = [data['weekId'],json];
                            db.query(sql,param,(err,res)=>{
                                if(err){
                                    console.log("Schedule Publish [4]",err);
                                    return rejectLocal(resCreator.resolveErrorAnswer(err.code));
                                }
                                return resolveLocal();
                            });
                        });
                        await new Promise((resolveLocal,rejectLocal)=>{
                            let sql = "UPDATE `weeks` SET `published`=1 WHERE `id` = ?";
                            let param = [data['weekId']];
                            db.query(sql,param,(err,res)=>{
                                if(err){
                                    console.log("Schedule Publish [5]",err);
                                    return rejectLocal(resCreator.resolveErrorAnswer(err.code));
                                }
                                return resolveLocal(resCreator.resolveAnswer());
                            });
                        });
                    })();
                }
            });
        });


    });
};

/**
 * Функция преобразования дня недели к формату {[день месяца] [месяц]}
 * @param {Date} startWeekDate - Дата начала недели
 * @param {number} dayOfWeek - Номер дня недели (с 1)
 * @returns {string|null} Строка формата - [день месяца] [месяц]
 */
function getDateString(startWeekDate,dayOfWeek) {
    let printDate = new Date(startWeekDate);
    printDate.setDate(printDate.getDate()+dayOfWeek-1);
    switch (printDate.getMonth()) {
        case 0:
            return `${printDate.getDate()} января`;
        case 1:
            return `${printDate.getDate()} февраля`;
        case 2:
            return `${printDate.getDate()} марта`;
        case 3:
            return `${printDate.getDate()} апреля`;
        case 4:
            return `${printDate.getDate()} мая`;
        case 5:
            return `${printDate.getDate()} июня`;
        case 6:
            return `${printDate.getDate()} июля`;
        case 7:
            return `${printDate.getDate()} августа`;
        case 8:
            return `${printDate.getDate()} сентября`;
        case 9:
            return `${printDate.getDate()} октября`;
        case 10:
            return `${printDate.getDate()} ноября`;
        case 11:
            return `${printDate.getDate()} декабря`;
        default:
            return null;
    }
}

/**
 * Фукнция генерации расписания в формате json
 * @param {array} res - Расписание (ответ от сервера)
 * @param {Date} startWeekDate  - Дата начала недели
 * @param {number} daysInWeek  - Дней в неделе
 * @returns {string} json
 */
function generateScheduleJson(res,startWeekDate,daysInWeek) {
    let jsonObject = {};
    res.forEach(value => {
        if (!jsonObject[value.groupName]) {
            jsonObject[value.groupName] = {};
            for(let day = 1; day <= daysInWeek; day++){
                jsonObject[value.groupName][""+day] = {};
                jsonObject[value.groupName][""+day].date = getDateString(startWeekDate,day);
                console.log(startWeekDate,value.day,getDateString(startWeekDate,day));
                for (let i = 1; i <= 6; i++) {
                    jsonObject[value.groupName][""+day][i] = {};
                    jsonObject[value.groupName][""+day][i]["lesson"] = null;
                    jsonObject[value.groupName][""+day][i]["lessonTeacher"] = null;
                    jsonObject[value.groupName][""+day][i]["lessonRoomNumber"] = null;
                }
            }
            console.log(jsonObject);
        }
        console.log(value);
        console.log(jsonObject[value.groupName][value.day][value.lessonNumber]);
        jsonObject[value.groupName][value.day][value.lessonNumber]["lesson"] = value.lesson;
        jsonObject[value.groupName][value.day][value.lessonNumber]["lessonTeacher"] = value.teacher;
        jsonObject[value.groupName][value.day][value.lessonNumber]["lessonRoomNumber"] = value.room;
    });
    console.log(jsonObject);
    return JSON.stringify(jsonObject);
}


