/**
 * Модуль для установления нового- или замены старого предмета
 * @param {number} weekId - Id недели
 * @param {number} day - Номер дня недели
 * @param {number} lessonNumber - Номер пары
 * @param {number} groupId - Id группы
 * @param {number} weekId - Id недели
 * @param {object} lesson - Предмет
 * @param {object} lesson.lessonOne - Первый предмет (id)
 * @param {object} lesson.lessonTwo - Второй предмет (id)
 * @param {object} lesson.roomOne - Первый кабинет (id)
 * @param {object} lesson.roomTwo - Второй кабинет (id)
 * @param {object} lesson.teacherOne - Первый учитель (id)
 * @param {object} lesson.teacherTwo - Второй учитель (id)
 * @param {object} lesson.type - Тип предмета (4 лекция на 2 подгруппы; 3 практика на 2 подгруппы; 2-практика 2 подгруппа; 1-практика 1 подгруппа; 0 - лекция)
 * @return {object}
 * @module scheduleSetLesson
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise(async (resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        await new Promise( (resolve, reject) => {
            let sql = "DELETE FROM `schedule` WHERE `id_week`=? AND `day`=? AND `lessonNumber`=? AND `id_group`=?";
            let param = [data['weekId'], data['day'], data['lessonNumber'], data['groupId']];
            db.query(sql, param, (err, res, fields) => {
                if (err) {
                    reject(resCreator.resolveErrorAnswer(err.code));
                } else {
                    resolve();
                }

            });
        });
        await new Promise( (resolve,reject) => {
            let sql = "INSERT INTO `schedule`(`id_acc`, `id_week`, `day`, `lessonNumber`, `id_group`, `id_item_p1`, `id_item_p2`, `id_teacher_one`, `id_teacher_two`, `id_room_p1`, `id_room_p2`, `type`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `id_item_p1`=?, `id_item_p2`=?, `id_teacher_one` =?, `id_teacher_two` =?, `id_room_p1`=?, `id_room_p2`=?, `type` =?";
            let param = [userId,data['weekId'],data['day'],data['lessonNumber'],data['groupId'],data['lesson']['lessonOne'],data['lesson']['lessonTwo'],data['lesson']['teacherOne'],data['lesson']['teacherTwo'],data['lesson']['roomOne'],data['lesson']['roomTwo'],data['lesson']['type'],data['lesson']['lessonOne'],data['lesson']['lessonTwo'],data['lesson']['teacherOne'],data['lesson']['teacherTwo'],data['lesson']['roomOne'],data['lesson']['roomTwo'],data['lesson']['type']];
            db.query(sql, param, (err) => {
                if (err) {
                    reject(resCreator.resolveErrorAnswer(err.code));
                } else {
                    return resCreator.resolveAnswer();
                }
            });
        });
    });
};


