/**
 * Модуль для получения расписания группы. Sql запрос лучше не смотреть...
 * @param {number} groupId - Id группы
 * @param {number} weekId - Id недели
 * @return {object}
 * @module scheduleGetGroup
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "SELECT `day`, `lessonNumber`, `id_group`, \
        CONCAT( (SELECT name FROM settings_items WHERE id = `id_item_p1` ), ' ',\
IF(`type` = 1 OR `type` = 3,'пр1 ',''), IF(`type` = 2,'пр2 ',''), \
        IF(`id_item_p2` IS NOT NULL,\
CONCAT( (SELECT name FROM settings_items WHERE id = `id_item_p2` ), ' ', \
IF(`type` = 2 OR `type` = 3,'пр2 ','') ) \
,'' ) ) AS lesson,\
        CONCAT( (SELECT name FROM settings_rooms WHERE id = `id_room_p1` ), ' ', \
        IF(`id_room_p2` IS NOT NULL, (SELECT name FROM settings_rooms WHERE id = `id_room_p2` ), '') ) AS room, \
        \
        CONCAT( \
IF(`type` = 2,\
   IF(\
       (SELECT id_teacher_two FROM settings_items WHERE id = `id_item_p1`) IS NOT NULL,\
       (SELECT CONCAT(last_name,' ',SUBSTRING(first_name,1,1),'.',SUBSTRING(father_name,1,1),'.') FROM settings_teachers WHERE `id` = (SELECT id_teacher_two FROM settings_items WHERE id = `id_item_p1` ) ),\
       (SELECT CONCAT(last_name,' ',SUBSTRING(first_name,1,1),'.',SUBSTRING(father_name,1,1),'.') FROM settings_teachers WHERE `id` = (SELECT id_teacher_one FROM settings_items WHERE id = `id_item_p1` ) )\
       ),\
   (SELECT CONCAT(last_name,' ',SUBSTRING(first_name,1,1),'.',SUBSTRING(father_name,1,1),'.') FROM settings_teachers WHERE `id` = (SELECT id_teacher_one FROM settings_items WHERE id = `id_item_p1` ) )\
   ),\
        ' ',\
        IF(`type` = 4,\
        IF((SELECT id_teacher_two FROM settings_items WHERE id = `id_item_p1` ) IS NOT NULL,\
        (SELECT CONCAT(last_name,' ',SUBSTRING(first_name,1,1),'.',SUBSTRING(father_name,1,1),'.') FROM settings_teachers WHERE `id` = (SELECT id_teacher_two FROM settings_items WHERE id = `id_item_p1` )), '')\
,''),\
        \
        If(`id_item_p2` IS NOT NULL,CONCAT(\
        (SELECT CONCAT(last_name,' ',SUBSTRING(first_name,1,1),'.',SUBSTRING(father_name,1,1),'.') FROM settings_teachers WHERE `id` = (SELECT id_teacher_one FROM settings_items WHERE id = `id_item_p2` ) ),\
        ' ',\
        \
        IF((SELECT id_teacher_two FROM settings_items WHERE id = `id_item_p2` ) IS NOT NULL,\
        (SELECT CONCAT(last_name,' ',SUBSTRING(first_name,1,1),'.',SUBSTRING(father_name,1,1),'.') FROM settings_teachers WHERE `id` = (SELECT id_teacher_two FROM settings_items WHERE id = `id_item_p2` )), '')\
        ),'')\
        ) AS teacher\
        \
        FROM `schedule` WHERE id_group = ? and id_week = ?";
        let param = [data['groupId'], data['weekId']];
        db.query(sql, param, (err, res) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer({groupId:data.groupId,schedule:res});
            }

        });
    });
};


