/**
 * Регистратор событий связанных с расписанием
 * @module scheduleMain
 * @param {registrator} registrator - Экземпляр регистратора
 */
module.exports = (registrator) =>{
    registrator.registAction('schedule-get-groups',require('./scheduleGetGroups'),{checkSession:true,fields:['semesterId']}); // событие получения списка групп
    registrator.registAction('schedule-get-group',require('./scheduleGetGroup'),{checkSession:true,fields:['weekId','groupId']}); // событие получения расписания группы
    registrator.registAction('schedule-get-lesson',require('./scheduleGetLesson'),{checkSession:true,fields:['weekId','groupId','semesterId','day','lessonNumber']}); // событие получения инфы о паре
    registrator.registAction('schedule-set-lesson',require('./scheduleSetLesson'),{checkSession:true,fields:['weekId','groupId','day','lessonNumber','lesson']}); // событие установки новой пары
    registrator.registAction('schedule-delete-lesson',require('./scheduleDeleteLesson'),{checkSession:true,fields:['weekId','groupId','day','lessonNumber']}); // событие удаления пары
    registrator.registAction('schedule-get-rooms-busy',require('./scheduleGetRoomsBusy'),{checkSession:true,fields:['weekId','semesterId']}); // событие получения информации о всех свободных/занятых кабинетах на неделе
    registrator.registAction('schedule-publish',require('./schedulePublish'),{checkSession:true,fields:['weekId']}); // событие публикации расписания

};