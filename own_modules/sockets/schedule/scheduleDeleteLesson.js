/**
 * Модуль для удаления пары из расписания
 * @param {number} weekId - Id недели
 * @param {number} day - Номер дня недели
 * @param {number} lessonNumber - Номер пары
 * @param {number} groupId - Id группы
 * @return {object}
 * @module scheduleDeleteLesson
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "DELETE FROM `schedule` WHERE `id_week`=? AND `day`=? AND `lessonNumber`=? AND `id_group`=?";
        let param = [data['weekId'],data['day'],data['lessonNumber'],data['groupId']];
        db.query(sql, param, (err) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer();
            }
        });
    });
};


