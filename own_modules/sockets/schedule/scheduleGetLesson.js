/**
 * Модуль для получения информации о паре
 * @param {number} semesterId - Id семестра
 * @param {number} day - Номер дня недели
 * @param {number} lessonNumber - Номер пары
 * @param {number} groupId - Id группы
 * @param {number} weekId - Id недели
 * @return {object}
 * @module scheduleGetLesson
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let answer = {};

        waitAll = [
            new Promise((resolve, reject) => {//информация об этой паре
                let sql = "SELECT `id_item_p1`,`id_item_p2`,`id_room_p1`,`id_room_p2`,`type` FROM `schedule` WHERE `day` = ? AND `lessonNumber` = ? AND `id_group` = ? AND `id_week` = ?";
                let param = [data['day'], data['lessonNumber'], data['groupId'], data['weekId']];
                db.query(sql, param, (err, res, fields) => {
                    if (err) {
                        reject(resCreator.resolveErrorAnswer(err.code));
                    } else {
                        if (res[0]) {
                            answer['lessonInfo'] = res[0];
                        } else {
                            answer['lessonInfo'] = null
                        }
                        resolve();
                    }

                });
            }),
            new Promise((resolve, reject) => {
                let sql = "SELECT settings_items.id, settings_items.name, id_teacher_one, id_teacher_two, \
        (SELECT CONCAT(last_name,' ',SUBSTRING(first_name,1,1),'.',SUBSTRING(father_name,1,1),'.') FROM settings_teachers WHERE id = `id_teacher_one`) as teacher_one , \
        IF(`id_teacher_two` IS NOT NULL, (SELECT CONCAT(last_name,' ',SUBSTRING(first_name,1,1),'.',SUBSTRING(father_name,1,1),'.') FROM settings_teachers WHERE id = `id_teacher_two`), NULL) AS teacher_two,\
        IF(EXISTS(SELECT * FROM schedule WHERE  ( (schedule.id_teacher_one = settings_items.id_teacher_one OR schedule.id_teacher_two = settings_items.id_teacher_one) OR (schedule.id_teacher_one = settings_items.id_teacher_two OR schedule.id_teacher_two = settings_items.id_teacher_two))  AND day = ? AND lessonNumber = ? AND id_week = ?),true,false) as busy,\
(SELECT id_room FROM settings_teachers WHERE id = `id_teacher_one` ) as room_one,\
(SELECT id_room FROM settings_teachers WHERE id = `id_teacher_two` ) as room_two,\
IF(`id_teacher_two` IS NOT NULL, true, false) as solo\
        FROM `settings_items` WHERE `id_group` = ? ORDER BY busy";//учителя
                let param = [data['day'], data['lessonNumber'], data['weekId'], data['groupId']];
                db.query(sql, param, (err, res, fields) => {
                    if (err) {
                        reject(resCreator.resolveErrorAnswer(err.code));
                    } else {
                        if (res)
                            answer['items'] = res;
                        else
                            answer['items'] = null;
                        resolve();

                    }

                });
            }),
            new Promise((resolve, reject) => {
                let sql = "Select @day:=?, @lessonNumber:=?, @id_week:=?, id,name,multiply,\
        IF(EXISTS(SELECT * FROM schedule WHERE (id_room_p1 = settings_rooms.id OR id_room_p2 = settings_rooms.id) and day = @day and lessonNumber = @lessonNumber  and id_week = @id_week ),true,false ) as busy, \
        IF(\
            (SELECT COUNT(*) from schedule WHERE (id_room_p1 = settings_rooms.id OR id_room_p2 = settings_rooms.id) and day = @day and lessonNumber = @lessonNumber and id_week = @id_week) > 1, \"MYLT\",\
        (SELECT `id_group` from schedule WHERE (id_room_p1 = settings_rooms.id OR id_room_p2 = settings_rooms.id) and day = @day and lessonNumber = @lessonNumber and id_week = @id_week LIMIT 1)) as id_group,\
                (SELECT COUNT(*) from schedule WHERE (id_room_p1 = settings_rooms.id OR id_room_p2 = settings_rooms.id) and day = @day and lessonNumber = @lessonNumber and id_week = @id_week) as groupsInRoom\
         FROM `settings_rooms` WHERE id_acc = ? and id_semester = ?";
                //Запрос выше: ...settings_rooms.id
                let param = [data['day'], data['lessonNumber'], data['weekId'], userId, data.semesterId];
                db.query(sql, param, (err, res) => {
                    if (err) {
                        reject(resCreator.resolveErrorAnswer(err.code));
                    } else {
                        if(res)
                            answer['rooms'] = res.map(value =>{
                                return {
                                    id:value.id,
                                    name:value.name,
                                    multiply: value.multiply,
                                    busy: value.busy,
                                    id_group: value.id_group,
                                    groupsInRoom:value.groupsInRoom,
                                };
                            });
                        else
                            answer['rooms'] = null;
                        resolve();
                    }
                });
            })
        ];

        Promise.all(waitAll).then(()=>resCreator.resolveAnswer(answer));//как всё придёт, отправляем ответ

    });
};


