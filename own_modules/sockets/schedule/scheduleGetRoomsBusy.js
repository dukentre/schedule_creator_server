/**
 * Модуль для получения информации о всех свободных/занятых кабинетах на неделе
 * @param {number} weekId - Id недели
 * @param {number} semesterId - Id семестра
 * @return {object}
 * @module scheduleGetRoomsBusy
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let answer = {};//объект ответа

        let queries = [];//массив обещаний
        for(let day=1; day<=7;day++){//дни недели
            answer[day] ={};//инициализируем новый день
            for(let lesson=1; lesson<=6;lesson++){
                answer[day][lesson] ={};//инициализируем новую пару в дне недели
                queries.push(new Promise((resolveLocal,rejectLocal)=>{//закидываем обещание с запросом
                    let sql = "SELECT id, name, IF(EXISTS(SELECT * FROM schedule WHERE (id_room_p1 = settings_rooms.id OR id_room_p2 = settings_rooms.id) and day = ? and lessonNumber = ?  and id_week = ? ),true,false ) as busy FROM `settings_rooms` WHERE `id_semester`=? and `id_acc`=? ORDER BY id";
                    let param = [day,lesson,data['weekId'],data['semesterId'],userId];
                    db.query(sql, param, (err,res) => {
                        if (err) {
                            rejectLocal(err.code);
                        } else {
                            answer[day][lesson] = res;
                            return resolveLocal();
                        }
                    });
                }))
            }
        }
        Promise.all(queries).then(()=>{//когда все запросы выполнены
            return resCreator.resolveAnswer(answer);
        }).catch((error)=>{//случилась ошибка в одном из запросов
            return resCreator.resolveErrorAnswer(error);
        });
    });
};


