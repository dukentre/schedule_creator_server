/**
 * Модуль для отправки сообщения об ошибке
 * @param {string} message - сообщение
 * @return {object}
 * @module scheduleSetLesson
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise(async (resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "INSERT INTO `reports`(`id_acc`, `message`) VALUES (?,?)";
        let param = [userId,data.message];
        db.query(sql,param, (err,res) => {
            if(err){
                return resCreator.resolveErrorAnswer(err.code);
            }
            return resCreator.resolveAnswer();
        })
    });
};


