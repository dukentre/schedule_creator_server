/**
 * Регистратор событий связанных с репортом
 * @module reportMain
 * @param {registrator} registrator - Экземпляр регистратора
 */
module.exports = (registrator) =>{
    registrator.registAction('reports-send',require('./reportsSend'),{checkSession:true,fields:['message']}); // событие отправки сообщения об ошибке

};