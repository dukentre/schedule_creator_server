/**
 * Класс, который генерирует ответ на запрос сокета в соответствии с принятыми стандартами
 * @class
 * @author Dukentre (vk.com/dukentreg || dukentre.ru)
 */
class ResolveCreator {
/*    resolveData;
    resolveCallback;*/

    /**
     * Принимает функцию Resolve от Promise, в котором вызывался
     * @param resolveCallback
     */
    constructor(resolveCallback) {
        this.resolveCallback = resolveCallback;
        this.resolveData = {};
    }

    //=============[Подготовка ответа, если он не соответствует стандарту]=================
    prepareData (data) {
        if (!data.data) {
            let answerData = {};
            answerData.err = false;
            answerData.message = 'success';
            answerData.data = data;
            return answerData;
        }
        return data;
    };

    /**
     * Добавляет в ответ вызов события для всех пользователей сайта
     * @param {string} action - Название события
     * @param {object} data - Объект с передаваемой информацией
     */
    addAllEmit(action, data) {
        if (!this.resolveData.emits) {
            this.resolveData.emits = [];
        }
        data = this.prepareData(data);
        this.resolveData.emits.push({
            action: action,
            all: true,
            data: data
        })
    }

    /**
     * Добавляет в ответ вызов события для всех сокетов в определённой комнате
     * @param {string} action - Название события
     * @param {number} roomId - Id комнаты
     * @param {object} data - Объект с передаваемой информацией
     */
    addRoomEmit(action, roomId, data) {
        if (!this.resolveData.emits) {
            this.resolveData.emits = [];
        }
        data = this.prepareData(data);
        this.resolveData.emits.push({
            action: action,
            roomId: roomId,
            data: data
        })
    }

    /**
     * Добавляет в ответ вызов события для того же пользователя, которому готовится ответ
     * @param {string} action - Название события
     * @param {object} data - Объект с передаваемой информацией
     */
    addEmit(action, data) {
        if (!'emits' in this.resolveData) {
            this.resolveData.emits = [];
        }
        data = this.prepareData(data);
        this.resolveData.emits.push({
            action: action,
            data: data
        })
    }

    /**
     * Метод для вызова удачного ответа пользователю
     * @param {object} data - Объект с передаваемой информацией
     */
    resolveAnswer(data) {
        this.resolveData.error = false;
        this.resolveData.message = 'success';
        this.resolveData.data = (data ? data : null);
        return  this.resolve();
    }

    /**
     * Метод для вызова ошибки ответа пользователю
     * @param {string} message - Код ошибки
     * @param {object} data - Объект с передаваемой информацией
     */
    resolveErrorAnswer(message, data) {
        this.resolveData.error = true;
        this.resolveData.message = message;
        this.resolveData.data = (data ? data : null);
        return  this.resolve();
    }

    /**
     * Метод для отправки ответа пользователю без создания специальной структуры
     */
    resolve() {
        if(typeof this.resolveCallback === 'function') {
            this.resolveCallback(this.resolveData);
        }else{
            return this.resolveData
        }
    }
}

module.exports = ResolveCreator;