/**
 * Регистратор событий связанных с учителями
 * @module settingsTeachersMain
 * @param {registrator} registrator - Экземпляр регистратора
 */
module.exports = (registrator) =>{
    registrator.registAction('settings-teachers-create',require('./settingsTeachersCreate'),{checkSession:true,fields:['firstName','lastName','fatherName','roomName','semesterId']}); // событие создания нового учителя
    registrator.registAction('settings-teachers-get',require('./settingsTeachersGet'),{checkSession:true,fields:['semesterId']}); // событие получения списка учителей
    registrator.registAction('settings-teachers-get-info',require('./settingsTeachersGetInfo'),{checkSession:true,fields: ['teacherId','semesterId']}); // событие получения информации об учителе
    registrator.registAction('settings-teachers-update',require('./settingsTeachersUpdate'),{checkSession:true,fields: ['firstName','lastName','fatherName','roomName','teacherId']}); // событие обновления информации об учителе
    registrator.registAction('settings-teachers-delete',require('./settingsTeachersDelete'),{checkSession:true,fields: ['teacherId']}); // событие удаления учителя

};