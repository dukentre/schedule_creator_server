/**
 * Модуль для удаления учителя
 * @param {number} teacherId - Название закреплённой комнаты
 * @return {object}
 * @module settingsTeachersDelete
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "DELETE FROM `settings_teachers` WHERE `id` =?";
        let param = [data.teacherId];
        db.query(sql, param, (err) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return  resCreator.resolveAnswer();
            }

        });
    });
};


