/**
 * Модуль для создания нового учителя
 * @param {string} firstName - Имя учителя
 * @param {string} lastName - Фамилия учителя
 * @param {string} fatherName - Отчество учителя
 * @param {string} roomName - Название закреплённой комнаты
 * @param {number} semesterId - Id семестра
 * @return {object}
 * @module settingsTeachersCreate
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "INSERT INTO `settings_teachers`(`id_acc`,`id_semester`, `first_name`, `last_name`, `father_name`, `id_room`) VALUES (?,?,?,?,?,(SELECT id FROM settings_rooms WHERE name = ? AND id_semester = ?))";
        let param = [userId,data.semesterId,data['firstName'],data['lastName'],data['fatherName'],data['roomName'],data.semesterId];
        db.query(sql, param, (err) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer();
            }

        });
    });
};


