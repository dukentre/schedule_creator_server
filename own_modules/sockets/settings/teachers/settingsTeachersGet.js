/**
 * Модуль для получения списка учителей
 * @param {number} semesterId - Id семестра
 * @return {object}
 * @module settingsTeachersGet
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "SELECT * FROM `settings_teachers` WHERE id_acc = ? and id_semester = ?";
        let param = [userId,data.semesterId];
        db.query(sql, param, (err, res) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer({teachers:res});
            }
        });
    });
};


