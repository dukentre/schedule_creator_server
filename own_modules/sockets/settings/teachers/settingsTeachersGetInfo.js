/**
 * Модуль для получения информации об учителе
 * @param {string} teacherId - Id учителя
 * @param {number} semesterId - id семестра
 * @return {object}
 * @module settingsTeachersGetInfo
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise(async (resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let answer = {};
        let idRoom = null;

        await new Promise(function(resolve, reject) {
            let sql = "SELECT * FROM `settings_teachers` WHERE id = ?";
            let param = [data['teacherId']];
            db.query(sql, param, (err, res) => {
                if (err) {
                    reject(resCreator.resolveErrorAnswer(err.code));
                } else {
                    if(res[0]) {
                        idRoom = res[0]['id_room'];
                        answer['first_name'] = res[0]['first_name'];
                        answer['last_name'] = res[0]['last_name'];
                        answer['father_name'] = res[0]['father_name'];
                        answer['id_room'] = res[0]['id_room']; // на будущее, чтобы можно было перейти к редактированию кабинета, мб[дата: хуй знает когда]
                        //16.02.20 я хз, что происходит в этом коде, я лишь занимаюсь рефакторингом
                        console.log(res[0]['last_name']);
                        resolve();
                    }else{
                        reject(resCreator.resolveErrorAnswer("TEACHER_NOT_FOUND"));
                    }
                }

            });
        });
        if(idRoom) {//если закреплен кабинет
            await new Promise(function (resolve, reject) {
                let sql = "SELECT * FROM `settings_rooms` WHERE `id` = ?";
                console.log("id room: ", idRoom);
                let param = [idRoom];
                db.query(sql, param, (err, res, fields) => {
                    if (err) {
                        reject(resCreator.resolveErrorAnswer(err.code));
                    } else {
                        //тут может быть ошибка в дб, поэтому ловим её
                        if(res[0]){//если кабинет найден
                            answer['roomName'] = res[0]['name'];//возможно отсутствует, если нет закрепленного кабинета
                        }else{
                            answer['roomName'] = null;
                        }
                        resolve();
                    }

                });
            });
        }
        let sql = "SELECT * FROM `settings_rooms` WHERE `id_semester` = ?";
        let param = [data.semesterId];
        db.query(sql, param, (err, res) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code)
            } else {
                answer['rooms'] = res;
                return resCreator.resolveAnswer(answer);
            }
        });

    });
};


