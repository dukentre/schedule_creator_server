/**
 * Модуль для сохранения настроек экспорта в эксель
 * @param {number} semesterId - Id семестра
 * @param {json} excelSettings - Настройки экспорта эксель
 * @return {object}
 * @module settingsExcelSave
 *
 */

const helper = require("./../../../helper");
const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        new Promise((resolveLocal,rejectLocal)=>{
            let sql = "DELETE FROM `settings_excel` WHERE `id_semester` = ?";
            let param = [data.semesterId];
            db.query(sql, param, (err, res) => {
                if (err) {
                   console.log("Delete Excel Settings:",err.code);
                }
                return resolveLocal();

            });
        }).then(()=>{
            let sql = "INSERT INTO `settings_excel`(`id_acc`, `id_semester`, `settings`) VALUES (?,?,?)";

            let param = [userId,data.semesterId,data.excelSettings];
            console.log(param);
            db.query(sql, param, (err, res) => {
                if (err) {
                    return resCreator.resolveErrorAnswer(err.code);
                } else {
                    return resCreator.resolveAnswer();
                }
            });
        })

    });
};


