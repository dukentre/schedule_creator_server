/**
 * Модуль для получения настроек excel
 * @param {number} semesterId - Id семестра
 * @return {object} Json настроек
 * @module settingsGroupsGet
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "SELECT `settings` FROM `settings_excel` WHERE id_semester = ?";
        let param = [data.semesterId];
        db.query(sql,param, (err,res) =>{
            if(err){
                return resCreator.resolveErrorAnswer(err.code);
            }
            if(res.length >0) {
                return resCreator.resolveAnswer({settings: res[0].settings});
            }else{
                return resCreator.resolveErrorAnswer("SETTINGS_NOT_FOUND");
            }
        })


    });
};


