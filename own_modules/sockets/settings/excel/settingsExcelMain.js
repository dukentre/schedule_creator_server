/**
 * Регистратор событий связанных с настройками экселя
 * @module settingsExcelMain
 * @param {registrator} registrator - Экземпляр регистратора
 */
module.exports = (registrator) =>{
    registrator.registAction('settings-excel-save',require('./settingsExcelSave'),{checkSession:true,fields:['semesterId','excelSettings']}); // событие сохранения настроек экспорта эксель
    registrator.registAction('settings-excel-get',require('./settingsExcelGet'),{checkSession:true,fields:['semesterId']}); // событие получения настроек эксель
};