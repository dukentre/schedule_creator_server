/**
 * Модуль для создания новой группы
 * @param {number} semesterId - Id семестра
 * @param {string} name - Название группы
 * @return {object} Информация о пользователе
 * @module settingsGroupsCreate
 *
 */

const helper = require("./../../../helper");
const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        console.log('create settings group');

        let sql = "INSERT INTO `settings_groups`(`id_acc`,`id_semester`, `name`) VALUES (?,?,?)";
        let param = [userId, data.semesterId, data['name']];
        db.query(sql, param, (err, res) => {
            if (err) {
                console.log(err);
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer();
            }
        });
    });
};


