/**
 * Модуль для обновления настроек группы
 * @param {string} name - Новое название группы
 * @param {string} id - Id группы в базе данных
 * @return {object}
 * @module settingsGroupsUpdate
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "UPDATE `settings_groups` SET `name`=? WHERE `id`=?";
        let param = [data['name'], data['id']];
        db.query(sql, param, (err, res) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer();
            }
        });
    });
};


