/**
 * Модуль для получения информации о группе
 * @param {string} id - Id группы в базе данных
 * @return {object} group_name, group_id, items
 * @module settingsGroupsGetInfo
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let answer = {};


        new Promise(function (resolve) {
            let sql = "SELECT * FROM `settings_groups` WHERE id = ?";
            let param = [data['id']];
            db.query(sql, param, (err, res) => {
                if (err) {
                    return resCreator.resolveErrorAnswer(err.code);
                } else {
                    answer['group_name'] = res[0]['name'];
                    answer['group_id'] = res[0]['id'];
                    resolve();
                }

            });
        }).then(() => {
            let sql = 'SELECT *, (SELECT  CONCAT(last_name," ",LEFT(first_name,1),".",LEFT(father_name,1),".")  FROM settings_teachers WHERE id = `id_teacher_one` ) as name_teacher_one, (SELECT  CONCAT(last_name," ",LEFT(first_name,1),".",LEFT(father_name,1),".")  FROM settings_teachers WHERE id = `id_teacher_two` ) as name_teacher_two  FROM `settings_items` WHERE `id_group` = ?';
            let param = [data['id']];
            db.query(sql, param, (err, res) => {
                if (err) {
                    return resCreator.resolveErrorAnswer(err.code);
                } else {
                    answer['items'] = res;
                    return resCreator.resolveAnswer(answer);
                }
            });
        });
    });
};


