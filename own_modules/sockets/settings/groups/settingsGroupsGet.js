/**
 * Модуль для получения списка групп
 * @return {object} Список групп и учителей
 * @module settingsGroupsGet
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let answer = {};

        new Promise(function (resolve, reject) {
            let sql = "SELECT * FROM `settings_groups` WHERE id_acc = ? AND id_semester = ?";
            let param = [userId,data.semesterId];
            db.query(sql, param, (err, res) => {
                if (err) {
                    reject(resCreator.resolveErrorAnswer(err.code));
                } else {
                    console.log("группы получены, отправляем!");
                    answer.groups = res;
                    resolve();
                }
            });
        })
        .then(() => {
                let sql = "SELECT * FROM `settings_teachers` WHERE id_acc = ? AND id_semester = ?";
                let param = [userId,data.semesterId];
                db.query(sql, param, (err, res, fields) => {
                    if (err) {
                        return resCreator.resolveErrorAnswer(err.code);
                    } else {
                        console.log("группы получены, отправляем!");
                        answer.teachers = res;
                        return resCreator.resolveAnswer(answer);
                    }
                });
            }
        );
    });
};


