/**
 * Модуль для удаления группы
 * @param {string} id - Id группы в базе данных
 * @return {object}
 * @module settingsGroupsDelete
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "DELETE FROM `settings_groups` WHERE `id` = ?";
        let param = [data.id];
        db.query(sql, param, (err, res) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code)
            } else {
                return resCreator.resolveAnswer();
            }

        });
    });
};


