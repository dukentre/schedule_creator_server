/**
 * Регистратор событий связанных с группами
 * @module settingsGroupsMain
 * @param {registrator} registrator - Экземпляр регистратора
 */
module.exports = (registrator) =>{
    registrator.registAction('settings-groups-get',require('./settingsGroupsGet'),{checkSession:true,fields:['semesterId']}); // событие получение групп
    registrator.registAction('settings-groups-create',require('./settingsGroupsCreate'),{checkSession:true,fields:['name','semesterId']}); // событие создания новой группы
    registrator.registAction('settings-groups-update',require('./settingsGroupsUpdate'),{checkSession:true,fields:['name','id']}); // событие обновления названия группы
    registrator.registAction('settings-groups-get-info',require('./settingsGroupsGetInfo'),{checkSession:true,fields:['id']}); // событие получения информации о группе
    registrator.registAction('settings-groups-delete',require('./settingsGroupsDelete'),{checkSession:true,fields:['id']}); // событие удаления группы и всех её предметов
};