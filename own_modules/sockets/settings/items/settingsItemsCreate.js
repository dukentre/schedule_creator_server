/**
 * Модуль для создания нового предмета у группы
 * @param {number} groupId - Id группы
 * @param {string} itemName - Название предмета
 * @param {number} itemTeacherOne - Id первого учителя у предмета
 * @param {number} itemTeacherTwo - Id второго учителя у предмета
 * @return {object} нихуя, ибо проектировал эту систему даун(я). Но у меня есть оправдание:
 * я этот проект начинал как изучение Node js и Vue js, поэтому мне было наплевать на архитектуру проекта.
 * И после изучения на неплохом уровне мне надоело, я пытался отказаться от этого мёртворождённого проекта,
 * но у Родина красноречие на 100 вкачено, ну и куда деваться. Поэтому, если тебе поручили модифицировать этот проект,
 * то я тебе сильно сочувствую. Лучше попробуй отказаться от этой затеи, это того точно не стоит.
 * @module settingsItemsCreate
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "INSERT INTO `settings_items`(`id_acc`,`id_group`, `name`, `id_teacher_one`, `id_teacher_two`) VALUES (?,?,?,?,?)";
        let param = [userId,data['groupId'],data['itemName'],data['itemTeacherOne'],data['itemTeacherTwo']];
        db.query(sql, param, (err) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer();
            }

        });
    });
};


