/**
 * Модуль для удаления предмета
 * @param {number} itemId - Id Предмета
 * @return {object}
 * @module settingsItemsDelete
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "DELETE FROM `settings_items` WHERE `id` = ?";
        let param = [data.itemId];
        db.query(sql, param, (err) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer();
            }
        });
    });
};


