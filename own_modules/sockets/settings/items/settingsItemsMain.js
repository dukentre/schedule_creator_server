/**
 * Регистратор событий связанных с предметами
 * @module settingsItemsMain
 * @param {registrator} registrator - Экземпляр регистратора
 */
module.exports = (registrator) =>{
    registrator.registAction('settings-items-create',require('./settingsItemsCreate'),{checkSession:true,fields:['groupId','itemName','itemTeacherOne','itemTeacherTwo']}); // событие создания нового предмета у группы
    registrator.registAction('settings-items-update',require('./settingsItemsUpdate'),{checkSession:true,fields:['itemId','itemName','itemTeacherOne','itemTeacherTwo']}); // событие изменения предмета у группы
    registrator.registAction('settings-items-delete',require('./settingsItemsDelete'),{checkSession:true,fields:['itemId']}); // событие удаления предмета у группы

};