/**
 * Модуль для изменения предмета у группы
 * @param {number} itemId - Id Предмета
 * @param {string} itemName - Название предмета
 * @param {number} itemTeacherOne - Id первого учителя
 * @param {number} itemTeacherTwo - Id второго учителя
 * @return {object}
 * @module settingsItemsUpdate
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "UPDATE `settings_items` SET `name`= ?,`id_teacher_one`=?,`id_teacher_two`=? WHERE `id`=?";
        let param = [data['itemName'],data['itemTeacherOne'],data['itemTeacherTwo'],data['itemId']];
        db.query(sql, param, (err) => {
            if (err) {
                resCreator.resolveErrorAnswer(err.code);
            } else {
                resCreator.resolveAnswer();
            }

        });
    });
};


