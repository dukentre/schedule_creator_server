/**
 * Модуль для удаления кабинета
 * @param {string} roomName - Название кабинета
 * @param {number} semesterId - Id семестра
 * @return {object}
 * @module settingsRoomsDelete
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "DELETE FROM `settings_rooms` WHERE `name` = ? and id_acc = ? and id_semester = ?";
        let param = [data.roomName,userId,data.semesterId];
        db.query(sql, param, (err) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return  resCreator.resolveAnswer();
            }
        });
    });
};


