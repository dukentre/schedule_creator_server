/**
 * Регистратор событий связанных с кабинетами
 * @module settingsRoomsMain
 * @param {registrator} registrator - Экземпляр регистратора
 */
module.exports = (registrator) =>{
    registrator.registAction('settings-rooms-create',require('./settingsRoomsCreate'),{checkSession:true,fields:['roomName','roomMultiply','semesterId']}); // событие создания нового кабинета
    registrator.registAction('settings-rooms-get',require('./settingsRoomsGet'),{checkSession:true,fields:['semesterId']}); // событие получения списка кабинетов
    registrator.registAction('settings-rooms-update',require('./settingsRoomsUpdate'),{checkSession:true,fields:['editedName','editedMultiply','roomName','semesterId']}); // событие обновления информации о кабинете
    registrator.registAction('settings-rooms-get-info',require('./settingsRoomsGetInfo'),{checkSession:true,fields:['roomId']}); // событие получения информации о кабинете
    registrator.registAction('settings-rooms-delete',require('./settingsRoomsDelete'),{checkSession:true,fields:['roomName','semesterId']}); // событие удаления комнаты

};