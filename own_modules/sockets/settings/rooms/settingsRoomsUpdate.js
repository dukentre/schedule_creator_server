/**
 * Модуль для обновления информации о кабинете
 * @param {string} editedName - Изменённое название кабинета
 * @param {0|1} editedMultiply - Можно ли ставить несколько групп
 * @param {string} roomName - Старое название кабинета
 * @param {number} semesterId - Id семестра
 * @return {object}
 * @module settingsRoomsUpdate
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "UPDATE `settings_rooms` SET `name` = ?,`multiply` = ? WHERE `name` = ? AND id_acc = ? AND id_semester = ?";
        let param = [data['editedName'],data['editedMultiply'],data['roomName'],userId,data.semesterId];
        db.query(sql, param, (err) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer()
            }

        });
    });
};


