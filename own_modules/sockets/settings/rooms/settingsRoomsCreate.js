/**
 * Модуль для создания нового кабинета
 * @param {string} roomName - Название комнаты
 * @param {number} semesterId - Id семестра
 * @param {0|1} roomMultiply - Можно ли ставить несколько групп
 * @return {object}
 * @module settingsRoomsCreate
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "INSERT INTO `settings_rooms`(`id_acc`,`id_semester`, `name`, `multiply`) VALUES (?,?,?,?)";
        let param = [userId,data.semesterId,data['roomName'],data['roomMultiply']];
        db.query(sql, param, (err) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return  resCreator.resolveAnswer();
            }

        });
    });
};


