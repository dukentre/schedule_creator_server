/**
 * Модуль для получения информации о кабинете
 * @param {number} roomId - Id кабинета
 * @return {object}
 * @module settingsRoomsGetInfo
 *
 */

const db = require('./../../../db');
const ResolveCreator = require("./../../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let answer = {};

        let waitDb = [//массив промисов, чтобы хоть как-то увеличить производительность
            new Promise((resolve, reject) => {
                let sql = "SELECT * FROM settings_rooms WHERE id = ?";
                db.query(sql, [data.roomId], (err, res) => {
                    if (err) {
                        return reject(resCreator.resolveErrorAnswer(err.code));
                    } else {
                        if(res[0]){//найдено
                            answer['name'] = res[0]['name'];
                            answer['multiply'] = res[0]['multiply'];
                            console.log(res[0]['name']);
                            resolve();
                        }
                        else return reject(resCreator.resolveErrorAnswer("ROOM_NOT_FOUND"));
                    }

                });
            }),
            new Promise((resolve, reject) => {
                let sql = "SELECT `first_name`,`last_name`,`father_name` FROM `settings_teachers` WHERE `id_room` = ?";
                db.query(sql, data.roomId, (err, res, fields) => {
                    if (err) {
                        return reject(resCreator.resolveErrorAnswer(err.code));
                    } else {
                        let teachers = [];
                        console.log(res);
                        for (teacher of res) {
                            //Тут [0] - первая буква имени и отчества
                            console.log(teacher['last_name'] + " " + teacher['first_name'][0] + "." + teacher['father_name'][0] + ".");
                            teachers.push(teacher['last_name'] + " " + teacher['first_name'][0] + "." + teacher['father_name'][0] + ".");
                        }
                        answer['teachers'] = teachers;
                        resolve();
                    }

                });
            })
        ];
        Promise.all(waitDb).then(() => resCreator.resolveAnswer(answer));


    });
};


