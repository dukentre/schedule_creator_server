/**
 * Модуль для получения списка семестров
 * @return {object}
 * @module semestersGet
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "SELECT * FROM semesters WHERE id_acc = ?";
        let param = [userId];
        db.query(sql, param, (err, res) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer({semesters:res});
            }
        });
    });
};


