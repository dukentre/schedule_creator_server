/**
 * Модуль для создания нового семестра
 * @param {string} semesterName - Название семестра
 * @return {object}
 * @module semestersCreate
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "INSERT INTO `semesters`(`id_acc`, `name`) VALUES (?,?)";
        let param = [userId,data['semesterName']];
        db.query(sql, param, (err) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer();
            }

        });
    });
};


