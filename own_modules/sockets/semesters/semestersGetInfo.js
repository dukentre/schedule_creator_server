/**
 * Модуль для получения информации о семестре(его названия)
 * @param {number} semesterId - Id семестра
 * @return {object}
 * @module semestersGetInfo
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "SELECT * FROM semesters WHERE id = ?";
        let param = [data['semesterId']];
        db.query(sql, param, (err, res) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                if (res[0]) {
                    return resCreator.resolveAnswer({
                        name: res[0]['name']
                    });
                } else {
                    return resCreator.resolveErrorAnswer("SEMESTER_NOT_FOUND");
                }

            }

        });
    });
};


