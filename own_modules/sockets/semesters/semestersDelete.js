/**
 * Модуль для удаления семестра
 * @param {number} semesterId - Id семестра
 * @return {object}
 * @module semestersDelete
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "DELETE FROM `semesters` WHERE `id` = ?";
        let param = [data.semesterId];
        db.query(sql, param, (err) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer();
            }

        });
    });
};


