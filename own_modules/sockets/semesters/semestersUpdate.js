/**
 * Модуль для обновления информации о семестре
 * @param {string} semesterName - Название семестра
 * @param {number} semesterId - Id семестра
 * @return {object}
 * @module semestersUpdate
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "UPDATE `semesters` SET `name`=? WHERE `id`=?";
        let param = [data['semesterName'],data['semesterId']];
        db.query(sql, param, (err) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer();
            }

        });
    });
};


