/**
 * Регистратор событий связанных с семестрами
 * @module semestersMain
 * @param {registrator} registrator - Экземпляр регистратора
 */
module.exports = (registrator) =>{
    registrator.registAction('semesters-get',require('./semestersGet'),{checkSession:true}); // событие получения списка семестров
    registrator.registAction('semesters-create',require('./semestersCreate'),{checkSession:true,fields:['semesterName']}); // событие создания нового семестра
    registrator.registAction('semesters-get-info',require('./semestersGetInfo'),{checkSession:true,fields:['semesterId']}); // событие получения информации о семестре
    registrator.registAction('semesters-update',require('./semestersUpdate'),{checkSession:true,fields:['semesterName','semesterId']}); // событие обновления информации о семестре
    registrator.registAction('semesters-delete',require('./semestersDelete'),{checkSession:true,fields:['semesterId']}); // событие удаления семестра

};