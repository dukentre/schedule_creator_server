/**
 * Модуль для создания новой недели
 * @param {number} weekId - Id Недели
 * @param {number} semesterId - Id Семестра
 * @param {boolean} regenerate - Пересоздать файл или нет
 * @return {object} Ссылка на файл
 * @module weeksDownloadExcel
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");
const fs = require("fs");
const sGenerator = require("./../../excel/scheduleGenerator");
module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        if(!data.regenerate) {
            fs.exists(__dirname + `../../../public_html/files/excelFiles/schedule_semester${data.semesterId}_week${data.weekId}.xlsx`, have => {
                if (have) {//файл уже найден
                    return resCreator.resolveAnswer({link: `/files/excelFiles/schedule_semester${semesterId}_week${weekId}.xlsx`});
                } else {//генерируем
                    createGenerateExcel();
                }
            });
        }else{
            createGenerateExcel();
        }


        function createGenerateExcel(){
            let exportSettings;
            let scheduleJson;

            new Promise((resolveLocal,rejectLocal) =>{
                let sql = "SELECT `settings` FROM `settings_excel` WHERE `id_semester` = ?";
                let param = [data.semesterId];
                db.query(sql, param, (err,res) => {
                    if (err) {
                        return rejectLocal(resCreator.resolveErrorAnswer(err.code));
                    } else if(res.length > 0) {
                        exportSettings = res[0].settings;
                        return resolveLocal();
                    }else {
                        return rejectLocal(resCreator.resolveErrorAnswer("SETTINGS_NOT_FOUND"));
                    }
                });
            }).then(()=>{
                let sql = "SELECT `json` FROM `schedule_json` WHERE `id_week` = ?";
                let param = [data.weekId];
                db.query(sql, param, (err,res) => {
                    if (err) {
                        return resCreator.resolveErrorAnswer(err.code);
                    } else if(res.length > 0) {
                        scheduleJson = res[0].json;
                        return resCreator.resolveAnswer({
                            link:sGenerator.GenerateScheduleWithSettings(data.weekId,data.semesterId,exportSettings,scheduleJson)//генерируем файл и отправляем ссылку
                        });
                    }else {
                        return resCreator.resolveErrorAnswer("JSON_NOT_FOUND");
                    }

                });
            })

        }

    });
};


