/**
 * Модуль для получения информации о неделе
 * @param {number} weekId - Id недели
 * @return {object}
 * @module weeksGetInfo
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "SELECT * FROM weeks WHERE id = ?";
        let param = [data['weekId']];
        db.query(sql, param, (err, res) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                if (res[0]) {//если неделя найдена
                    return resCreator.resolveAnswer({
                        start: res[0]['start'],
                        end: res[0]['end'],
                        published: res[0]['published']
                    });
                } else return resCreator.resolveErrorAnswer("WEEK_NOT_FOUND");
            }
        });
    });
};


