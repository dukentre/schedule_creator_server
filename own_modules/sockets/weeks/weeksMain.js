/**
 * Регистратор событий связанных с неделями
 * @module semestersMain
 * @param {registrator} registrator - Экземпляр регистратора
 */
module.exports = (registrator) =>{
    registrator.registAction('weeks-get',require('./weeksGet'),{checkSession:true,fields:['semesterId']}); // событие получения списка недель в семестре
    registrator.registAction('weeks-create',require('./weeksCreate'),{checkSession:true,fields:['semesterId','start','end']}); // событие создания новой недели
    registrator.registAction('weeks-update',require('./weeksUpdate'),{checkSession:true,fields:['weekId','start','end']}); // событие обновления информации о неделе
    registrator.registAction('weeks-get-info',require('./weeksGetInfo'),{checkSession:true,fields:['weekId']}); // событие получения информации о недели
    registrator.registAction('weeks-delete',require('./weeksDelete'),{checkSession:true,fields:['weekId']}); // событие удаления недели
    registrator.registAction('weeks-download-excel',require('./weeksDownloadExcel'),{checkSession:true,fields:['weekId','semesterId','regenerate']}); // событие удаления недели

};