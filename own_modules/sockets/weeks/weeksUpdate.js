/**
 * Модуль для изменения недели
 * @param {number} weekId - Id недели
 * @param {string} start - Дата начала недели
 * @param {string} end - Дата конца недели
 * @return {object}
 * @module weeksUpdate
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "UPDATE `weeks` SET `start`=?, `end`=? WHERE `id`=?";
        let param = [data['start'],data['end'],data['weekId']];
        db.query(sql, param, (err) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer();
            }

        });
    });
};


