/**
 * Модуль для создания новой недели
 * @param {number} semesterId - Id Семестра
 * @param {string} start - Дата начала недели
 * @param {string} end - Дата конца недели
 * @return {object}
 * @module weeksCreate
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "INSERT INTO `weeks`(`id_acc`, `id_semester`, `start`, `end`) VALUES (?,?,?,?)";
        let param = [userId,data['semesterId'],data['start'],data['end']];
        db.query(sql, param, (err) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer();
            }

        });
    });
};


