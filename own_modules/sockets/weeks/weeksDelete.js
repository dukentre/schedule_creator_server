/**
 * Модуль для удаления недели
 * @param {number} weekId - Id недели
 * @return {object}
 * @module weeksCreate
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "DELETE FROM `weeks` WHERE `id` = ?";
        let param = [data['weekId']];
        db.query(sql, param, (err, res, fields) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer();
            }
        });
    });
};


