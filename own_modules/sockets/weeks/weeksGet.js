/**
 * Модуль для получения списка недель в семестре
 * @param {number} semesterId - Id Семестра
 * @return {object}
 * @module weeksGet
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let sql = "SELECT * FROM weeks WHERE id_acc = ? and id_semester = ?";
        let param = [userId,data['semesterId']];
        db.query(sql, param, (err, res) => {
            if (err) {
                return resCreator.resolveErrorAnswer(err.code);
            } else {
                return resCreator.resolveAnswer({weeks:res});
            }
        });
    });
};


