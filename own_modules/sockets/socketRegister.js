const ResolveCreator = require("./resolveCreator");
const helper = require("./../helper");
/**
 * Регистратор сокетов, который регистрирует события и рассылает ответы на эти запросы
 * @class SocketRegister
 * @author Dukentre (vk.com/dukentreg || dukentre.ru)
 */
class SocketRegister {
    /**
     *
     * @param {socket} socket - Сокет пользователя, для которого будут регистрироваться события
     * @param io - Socket.io для глобальных событий
     * @param {string} checkSessionFieldName - название поля, по которому определяется наличие сессии
     */
    constructor(socket, io,checkSessionFieldName) {
        if (socket && io && checkSessionFieldName) {
            this.socket = socket;
            this.io = io;
            this.checkSessionFieldName = checkSessionFieldName;
        }
    }

    /**
     * Метод регистрации события
     * @param {string} action - Название событие
     * @param {function} callback - Функция обработки события
     * @param {object} params - Параметры
     * @param {boolean} params.checkSession - проверять ли сессию
     * @param {array<string>} params.fields - проверяет наличие полей
     * @param {object} params.checkSessionParams - проверяет состояние параметров в сессии
     * @example
     * registrator.registAction('settings-groups-update',require('./settingsGroupsUpdate'),{checkSession:true,fields:['name','id']}); // событие обновления названия группы
     */
    registAction(action, callback,params ={}) {
        this.socket.on(action, async data => {
            let answer = null;
            if(params && params.checkSession === true){//проверяем сессию
                if(!this.socket.handshake.session[this.checkSessionFieldName]){
                    answer = new ResolveCreator().resolveErrorAnswer("CONNECTION_ERROR");
                }
            }
            if(params && params.fields){//проверяем поля в данных от пользователя
                if(!helper.isExist(data,params.fields)){
                    answer = new ResolveCreator().resolveErrorAnswer("FIELDS_ERROR");
                }
            }
            if(params && params.checkSessionParams){//проверяем совпадение параметров сессии
                for(let param in params.checkSessionParams){
                    if(this.socket.handshake.session[param] !== params.checkSessionParams[param]){
                        answer = new ResolveCreator().resolveErrorAnswer("NO_ACCESS_ERROR");
                    }
                }
            }
            if(answer === null){
                answer = await callback(this.socket, this.io, data);//получем ответ
            }

            if ('emits' in answer) {//если есть дополнительные вызовы событий
                sendEmits(answer);
                delete answer['emits'];
            }
            this.socket.emit(action + '-answer', answer);//посылаем ответ на событие
        });

        let classThis = this;

        /**
         * Функция для обработки дополнительных вызовов событий, которые пришли от ответа
         * @param {object} answer - Ответ от функции обработчика
         */
        function sendEmits(answer) {
            for (let emit of answer.emits) {
                if ("all" in emit && emit.all == true) {//если сообщение для всех
                    classThis.io.emit(emit.action, emit.data);
                } else if ('roomId' in emit && emit.roomId) {//если сообщение для комнаты
                    classThis.socket.to(emit.roomId).emit(emit.action, emit.data)
                } else
                    classThis.socket.emit(emit.action, emit.data);
            }
        }
    }

}


module.exports = SocketRegister;