/**
 * Модуль для анализа кол-ва пар у подгрупп
 * @param {number} weekId - Id недели
 * @param {number} semesterId - Id семестра
 * @return {object}
 * @module analysisSubgroupsLessonsCount
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;


        new Promise((resolveLocal, rejectLocal) => {
            let sql = "SELECT `start`,`end` FROM `weeks` WHERE id = ?";
            param = [data['weekId']];
            db.query(sql, param, (err, res) => {
                if (err) {
                    return rejectLocal(resCreator.resolveErrorAnswer(err.code));
                } else {
                    if (res.length > 0) {
                        let first = new Date(res[0].start);
                        let second = new Date(res[0].end);
                        daysInWeek = new Date(second - first).getDate();//кол-во дней в неделе
                        return resolveLocal(daysInWeek);
                    }else{
                        rejectLocal(resCreator.resolveErrorAnswer("WEEK_NOT_FOUND"));
                    }
                }
            })
        }).then((daysInWeek)=>{
            let sql = `SELECT @weekId:=?,\`id\`,\`name\`,
     (SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 1 and id_week=@weekId and (type != 2 )) as mondaySubgroupOne,
     (SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 1 and id_week=@weekId and (type != 1 )) as mondaySubgroupTwo,
     (SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 2 and id_week=@weekId and (type != 2 )) as tuesdaySubgroupOne,
     (SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 2 and id_week=@weekId and (type != 1 )) as tuesdaySubgroupTwo,
     (SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 3 and id_week=@weekId and (type != 2 )) as wednesdaySubgroupOne,
     (SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 3 and id_week=@weekId and (type != 1 )) as wednesdaySubgroupTwo,
     (SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 4 and id_week=@weekId and (type != 2 )) as thursdaySubgroupOne,
     (SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 4 and id_week=@weekId and (type != 1 )) as thursdaySubgroupTwo,
     (SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 5 and id_week=@weekId and (type != 2 )) as fridaySubgroupOne,
     (SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 5 and id_week=@weekId and (type != 1 )) as fridaySubgroupTwo,
     (SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 6 and id_week=@weekId and (type != 2 )) as saturdaySubgroupOne,
     (SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 6 and id_week=@weekId and (type != 1 )) as saturdaySubgroupTwo,
     (SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 7 and id_week=@weekId and (type != 2 )) as sundaySubgroupOne,
     (SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 7 and id_week=@weekId and (type != 1 )) as sundaySubgroupTwo
               FROM \`settings_groups\` WHERE \`id_semester\` = ?`;
            let param = [data['weekId'], data['semesterId']];
            db.query(sql, param, (err,res) => {
                if (err) {
                    return resCreator.resolveErrorAnswer(err.code);
                } else {
                    return resCreator.resolveAnswer(analysis(res,daysInWeek));
                }
            });
        })

    });
};

function analysis(res,daysInWeek) {
    console.log("Составляем ответ анализа пар у подгрупп");
    let answer = {};
    for(let groupNum = 0; groupNum < res.length; groupNum++){//проходимся по всем группам
        if(res[groupNum].mondaySubgroupOne < 3 || res[groupNum].mondaySubgroupOne > 4){addGroupToAnswer(res[groupNum].name,'1',1, res[groupNum].mondaySubgroupOne)}
        if(res[groupNum].mondaySubgroupTwo < 3 || res[groupNum].mondaySubgroupTwo > 4){addGroupToAnswer(res[groupNum].name,'1',2, res[groupNum].mondaySubgroupTwo)}
        if(res[groupNum].tuesdaySubgroupOne < 3 || res[groupNum].tuesdaySubgroupOne > 4){addGroupToAnswer(res[groupNum].name,'2',1, res[groupNum].tuesdaySubgroupOne)}
        if(res[groupNum].tuesdaySubgroupTwo < 3 || res[groupNum].tuesdaySubgroupTwo > 4){addGroupToAnswer(res[groupNum].name,'2',2, res[groupNum].tuesdaySubgroupTwo)}
        if(res[groupNum].wednesdaySubgroupOne < 3 || res[groupNum].wednesdaySubgroupOne > 4){addGroupToAnswer(res[groupNum].name,'3',1, res[groupNum].wednesdaySubgroupOne)}
        if(res[groupNum].wednesdaySubgroupTwo < 3 || res[groupNum].wednesdaySubgroupTwo > 4){addGroupToAnswer(res[groupNum].name,'3',2, res[groupNum].wednesdaySubgroupTwo)}
        if(res[groupNum].thursdaySubgroupOne < 3 || res[groupNum].thursdaySubgroupOne > 4){addGroupToAnswer(res[groupNum].name,'4',1, res[groupNum].thursdaySubgroupOne)}
        if(res[groupNum].thursdaySubgroupTwo < 3 || res[groupNum].thursdaySubgroupTwo > 4){addGroupToAnswer(res[groupNum].name,'4',2, res[groupNum].thursdaySubgroupTwo)}
        if(res[groupNum].fridaySubgroupOne < 3 || res[groupNum].fridaySubgroupOne > 4){addGroupToAnswer(res[groupNum].name,'5',1, res[groupNum].fridaySubgroupOne)}
        if(res[groupNum].fridaySubgroupTwo < 3 || res[groupNum].fridaySubgroupTwo > 4){addGroupToAnswer(res[groupNum].name,'5',2, res[groupNum].fridaySubgroupTwo)}
        if((res[groupNum].saturdaySubgroupOne < 3 || res[groupNum].saturdaySubgroupOne > 4) && daysInWeek>5){addGroupToAnswer(res[groupNum].name,'6',1, res[groupNum].saturdaySubgroupOne)}
        if((res[groupNum].saturdaySubgroupTwo < 3 || res[groupNum].saturdaySubgroupTwo > 4) && daysInWeek>5){addGroupToAnswer(res[groupNum].name,'6',2, res[groupNum].saturdaySubgroupTwo)}
        if((res[groupNum].sundaySubgroupOne < 3 || res[groupNum].sundaySubgroupOne > 4) && daysInWeek>6){addGroupToAnswer(res[groupNum].name,'7',1, res[groupNum].sundaySubgroupOne)}
        if((res[groupNum].sundaySubgroupTwo < 3 || res[groupNum].sundaySubgroupTwo > 4) && daysInWeek>6){addGroupToAnswer(res[groupNum].name,'7',2, res[groupNum].sundaySubgroupTwo)}
    }
    return answer;


    function addGroupToAnswer(name,dayNumber,subgroup,countLessons) {
        if(!answer[name]){
            answer[name] = {};
            answer[name].groupName = name;
            answer[name].days = [];
        }
        answer[name].days.push({dayNumber,subgroup,countLessons});
    }
}


