/**
 * Модуль для анализа окон в расписании
 * @param {number} weekId - Id недели
 * @return {object}
 * @module analysisScheduleWindows
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

let dataBeforeAnalysis = {};
let answer = {};

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let queries = [];

        new Promise((resolveLocal, rejectLocal) => {
            let sql = "SELECT `start`,`end` FROM `weeks` WHERE id = ?";
            param = [data['weekId']];
            db.query(sql, param, (err, res) => {
                if (err) {
                    return rejectLocal(resCreator.resolveErrorAnswer(err.code));
                } else {
                    if (res.length > 0) {
                        let first = new Date(res[0].start);
                        let second = new Date(res[0].end);
                        daysInWeek = new Date(second - first).getDate();//кол-во дней в неделе
                        return resolveLocal(daysInWeek);
                    }else{
                        rejectLocal(resCreator.resolveErrorAnswer("WEEK_NOT_FOUND"));
                    }
                }
            })
        }).then((daysInWeek)=>{
            for (let day = 1; day <=daysInWeek; day++) {
                let sql = "SELECT `lessonNumber`, (SELECT name FROM settings_groups WHERE id = schedule.id_group) as groupName FROM `schedule` WHERE id_week = ? and day= ? AND type != ? ORDER BY `id_group`,`lessonNumber`"
                queries.push(new Promise((resolveLocal, rejectLocal) => {
                    let param = [data['weekId'],day,1];
                    db.query(sql,param,(err,res)=>{
                        if(err){
                            rejectLocal(resCreator.resolveErrorAnswer(err.code));
                        }else{
                            analysis(res,day,1);
                            resolveLocal();
                        }
                    })
                }));
                queries.push(new Promise((resolveLocal, rejectLocal) => {
                    let param = [data['weekId'],day,2];
                    db.query(sql,param,(err,res)=>{
                        if(err){
                            rejectLocal(resCreator.resolveErrorAnswer(err.code));
                        }else{
                            analysis(res,day,2);
                            resolveLocal();
                        }
                    })
                }));
            }

            Promise.all(queries).then(()=>{
                return resCreator.resolveAnswer(answer);
            })
        })

    });
};

function analysis(res,day,subgroup) {
    for(let lessonNum = 0; lessonNum < res.length; lessonNum++){//проходимся по всем группам
        preparing(lessonNum);
        let nextLesson = dataBeforeAnalysis[day][res[lessonNum].groupName+"-"+subgroup]+1;
        console.log(res[lessonNum].groupName,nextLesson,res[lessonNum].lessonNumber);
        if(nextLesson < res[lessonNum].lessonNumber){//если есть дыра в расписании
            addGroupToAnswer(res[lessonNum].groupName,day,subgroup,nextLesson);
        }else{//иначе устанавливаем номер следующей пары
            dataBeforeAnalysis[day][res[lessonNum].groupName+"-"+subgroup] = res[lessonNum].lessonNumber;
        }
    }

    function preparing(lessonNum) {//подготавливаем данные к анализу
        if(!dataBeforeAnalysis[day]){
            dataBeforeAnalysis[day] = {};
        }
        if(!dataBeforeAnalysis[day][res[lessonNum].groupName+"-"+subgroup]){
            dataBeforeAnalysis[day][res[lessonNum].groupName+"-"+subgroup] = res[lessonNum].lessonNumber;
        }
    }

    function addGroupToAnswer(name,dayNumber,subgroup,lesson) {
        if(!answer[name]){
            answer[name] = {};
            answer[name].groupName = name;
            answer[name].days = {};
        }
        if(!answer[name]['days'][dayNumber]){
            answer[name]['days'][dayNumber] ={};
        }
        if(!answer[name]['days'][dayNumber][subgroup]){
            answer[name]['days'][dayNumber][subgroup] ={dayNumber,subgroup,lesson};
        }
    }
}


