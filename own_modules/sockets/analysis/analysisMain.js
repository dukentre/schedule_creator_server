/**
 * Регистратор событий связанных с анализом расписания
 * @module analysisMain
 * @param {registrator} registrator - Экземпляр регистратора
 */
module.exports = (registrator) =>{
    registrator.registAction('analysis-empty-days',require('./analysisEmptyDays'),{checkSession:true,fields:['weekId','semesterId']}); // событие анализа пустых дней
    registrator.registAction('analysis-subgroups-lessons-count',require('./analysisSubgroupsLessonsCount'),{checkSession:true,fields:['weekId','semesterId']}); // событие анализа кол-ва пар у подгрупп
    registrator.registAction('analysis-groups-in-rooms',require('./analysisGroupsInRooms'),{checkSession:true,fields:['weekId','semesterId']}); // событие анализа кол-ва групп в кабинетах
    registrator.registAction('analysis-schedule-windows',require('./analysisScheduleWindows'),{checkSession:true,fields:['weekId']}); // событие анализа наличия окон в расписании

};