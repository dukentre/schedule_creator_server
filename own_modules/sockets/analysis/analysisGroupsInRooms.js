/**
 * Модуль для анализа кол-ва групп в кабинетах
 * @param {number} weekId - Id недели
 * @param {number} semesterId - Id семестра
 * @return {object}
 * @module analysisGroupsInRooms
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");
let answer = {};//объект, в котором готовится ответ

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;

        let queries = [];

        new Promise((resolveLocal, rejectLocal) => {//получаем кол-во дней в неделе
            let sql = "SELECT `start`,`end` FROM `weeks` WHERE id = ?";
            let param = [data['weekId']];
            db.query(sql, param, (err, res) => {
                if (err) {
                    return rejectLocal(resCreator.resolveErrorAnswer(err.code));
                } else {
                    if (res.length > 0) {//если неделя найдена
                        let first = new Date(res[0].start);
                        let second = new Date(res[0].end);
                        let daysInWeek = new Date(second - first).getDate();//кол-во дней в неделе
                        console.log(res,res.start,first,res.end,second,daysInWeek);
                        return resolveLocal(daysInWeek);
                    }else{
                        rejectLocal(resCreator.resolveErrorAnswer("WEEK_NOT_FOUND"));
                    }
                }
            })
        }).then((daysInWeek) => {
            let sql = `Select @day:=?, @lessonNumber:=?, @id_week:=?, id,name,multiply,
                (SELECT COUNT(*) from schedule WHERE (id_room_p1 = settings_rooms.id OR id_room_p2 = settings_rooms.id) and day = @day and lessonNumber = @lessonNumber and id_week = @id_week) as groupsInRoom
         FROM \`settings_rooms\` WHERE id_semester = ?`;//запрос на кол-во групп в кабинете в определённый день и пару
            for (let day = 1; day <= daysInWeek; day++) {//проходимся по дням в неделе
                for (let lesson = 1; lesson <= 6; lesson++) {//проходимся по парам в дне
                    queries.push(new Promise((resolveLocal, rejectLocal) => {//добавляем новый запрос в общий пул
                        let param = [day, lesson, data["weekId"], data["semesterId"]];
                        db.query(sql, param, (err, res) => {
                            if (err) {
                                return rejectLocal(resCreator.resolveErrorAnswer(err.code));
                            } else {
                                analysis(res,day,lesson);//проводим анализ этого дня и пары
                                return resolveLocal();
                            }
                        })
                    }));
                }
            }

            Promise.all(queries).then(()=>{//все запросы отработали
                return resCreator.resolveAnswer(answer);//отправляем ответ
            });
        });


    });
};


function analysis(res,dayNumber,lesson) {
    for (let roomNum = 0; roomNum < res.length; roomNum++) {//проходимся по всем комнатам
       if(res[roomNum].multiply === 0 && res[roomNum].groupsInRoom > 1){
           addGroupToAnswer(res[roomNum].name,res[roomNum].groupsInRoom)
       }
       else if(res[roomNum].multiply === 1 && res[roomNum].groupsInRoom > 2){
           addGroupToAnswer(res[roomNum].name,res[roomNum].groupsInRoom)
       }
    }

    function addGroupToAnswer(name,groupsInRoom) {
        if (!answer[name]) {
            answer[name] = {};
            answer[name].roomName = name;
            answer[name].days = {};
        }
        if(!answer[name]['days'][dayNumber]){
            answer[name]['days'][dayNumber] = {};
        }

        answer[name]['days'][dayNumber][lesson]={dayNumber,lesson,groupsInRoom};
    }
}


