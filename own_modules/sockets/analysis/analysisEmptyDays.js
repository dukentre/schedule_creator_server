/**
 * Модуль для анализа пустых дней
 * @param {number} weekId - Id недели
 * @param {number} semesterId - Id семестра
 * @return {object} Группа => {Номера дней = true}
 * @module analysisEmptyDays
 *
 */

const db = require('./../../db');
const ResolveCreator = require("./../resolveCreator");

module.exports = (socket, io, data) => {
    return new Promise((resolve) => {
        const resCreator = new ResolveCreator(resolve);
        let userId = socket.handshake.session.userId;


        new Promise((resolveLocal, rejectLocal) => {
            let sql = "SELECT `start`,`end` FROM `weeks` WHERE id = ?";
            param = [data['weekId']];
            db.query(sql, param, (err, res) => {
                if (err) {
                    return rejectLocal(resCreator.resolveErrorAnswer(err.code));
                } else {
                    if (res.length > 0) {
                        let first = new Date(res[0].start);
                        let second = new Date(res[0].end);
                        daysInWeek = new Date(second - first).getDate();//кол-во дней в неделе
                        return resolveLocal(daysInWeek);
                    }else{
                        rejectLocal(resCreator.resolveErrorAnswer("WEEK_NOT_FOUND"));
                    }
                }
            })
        }).then((daysInWeek)=>{
            let sql = "SELECT @weekId:=?,`id`,`name`,\n" +
                "(SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 1 and id_week=@weekId) as monday,\n" +
                "(SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 2 and id_week=@weekId) as tuesday,\n" +
                "(SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 3 and id_week=@weekId) as wednesday,\n" +
                "(SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 4 and id_week=@weekId) as thursday,\n" +
                "(SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 5 and id_week=@weekId) as friday,\n" +
                "(SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 6 and id_week=@weekId) as saturday,\n" +
                "(SELECT COUNT(*) FROM schedule WHERE id_group = settings_groups.id and day = 7 and id_week=@weekId) as sunday\n" +
                "FROM `settings_groups` WHERE `id_semester` = ?";
            let param = [data['weekId'], data['semesterId']];
            db.query(sql, param, (err,res) => {
                if (err) {
                    return resCreator.resolveErrorAnswer(err.code);
                } else {
                    return resCreator.resolveAnswer(analysis(res,daysInWeek));
                }
            });
        })

    });
};

function analysis(res,daysInWeek) {
    console.log("Составляем ответ анализа пустых дней");
    let answer = {};
    for(let groupNum = 0; groupNum < res.length; groupNum++){//проходимся по всем группам
        if(res[groupNum].monday === 0){addGroupToAnswer(res[groupNum].name,'1')}
        if(res[groupNum].tuesday === 0){addGroupToAnswer(res[groupNum].name,'2')}
        if(res[groupNum].wednesday === 0){addGroupToAnswer(res[groupNum].name,'3')}
        if(res[groupNum].thursday === 0){addGroupToAnswer(res[groupNum].name,'4')}
        if(res[groupNum].friday === 0){addGroupToAnswer(res[groupNum].name,'5')}
        if(res[groupNum].saturday === 0 && daysInWeek >= 6){addGroupToAnswer(res[groupNum].name,'6')}
        if(res[groupNum].sunday === 0 && daysInWeek >= 7){addGroupToAnswer(res[groupNum].name,'7')}
    }
    return answer;


    function addGroupToAnswer(name,dayNumber) {
        if(!answer[name]){
            answer[name] = {};
            answer[name].groupName = name;
            answer[name].days = [];
        }
        answer[name].days.push(dayNumber);
    }
}


