/**
 * Модуль для подключения других сокетов, выполняется один раз при старте сервера.
 * Также содержит:
 *
 * Событие Socket.io connection и socket disconnect.
 * @module mainSocket
 *
 */

//Пожалуйста, разделяйте события socket io файлам в папке own_modules/sockets. Стремимся к лёгкой поддержке кода.
const db = require('./../db');
const socketRegister = require("./socketRegister");

module.exports = (io) => {

    io.on('connection', (socket) => {
        console.log("connection!");
        socket.on('disconnect', (reason) => {

        });

        //=========[Инициализируем регистратор и передаём его дочерним файлам с событиями]=========
        let registrator = new socketRegister(socket, io,'userId');
        require('./auth/authMain')(registrator);
        require('./settings/groups/settingsGroupsMain')(registrator);
        require('./settings/items/settingsItemsMain')(registrator);
        require('./settings/rooms/settingsRoomsMain')(registrator);
        require('./settings/teachers/settingsTeachersMain')(registrator);
        require('./semesters/semestersMain')(registrator);
        require('./weeks/weeksMain')(registrator);
        require('./schedule/scheduleMain')(registrator);
        require('./analysis/analysisMain')(registrator);
        require('./settings/excel/settingsExcelMain')(registrator);
        require('./reports/reportsMain')(registrator);
    });
};