/**
 * Вспомогательный модуль, добавляющий некоторые мелкие функции
 * @module helper
 *
 */

const db = require("./db");


/**
 * Проверяет наличие полей (fields) в данных(data)
 * @param {object} data - Объект в котором ищем нужные поля
 * @param {array|string} fields - Поля, которые могут передаваться в виде массива или строкой(1 параметр)
 * @returns {boolean} Есть ли эти поля или нет
 * @author Алексей Крикинов, модифицировано Артемом Дукентровым
 * @example
 * isExist({dukentre:'288',Test:228},["dukentre","Test"]) => true
 */
function isExist(data, fields = []) {
    let isArray = Array.isArray(data);// проверка это массив или объект
    let all = Boolean(data);
    if (isArray) {
        all = all && data.length > 0;// проверяем пустой ли массив
        if (typeof all !== 'undefined') {// если не пустой
            data = data[0];// приводим массив к объекту
        }
    }
    if (all) {
        fields.forEach(field => {
            let inData = false;
            try {
                inData = field in data;// если поля нет в data, то вылитет в catch
                // или по какой-то другой причине уже вылетало, не помню
            } catch (e) {
                console.log(e);
            }
            all = all && inData;// а так красиво, как только all станет false, то дальше он всегда будет false
        });
    }
    return all;
}

module.exports = {
    isExist:isExist,
};