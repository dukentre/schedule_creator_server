/**
 * Модуль для работы с базой данных
 * @module db
 */
const mysql = require('mysql');


//==============[Настройки подключения к бд]==================
/*let mysqlSettings = {
    host     : 'localhost',
    user     : 's92073dj_schedul',
    password : 'qbt&sG10',
    database : 's92073dj_schedul'
};/**/

let mysqlSettings = {
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 's92073dj_schedul'
};/**/

/**
 * Функия выполняющая запрос в базу данных
 * @param {string} sql - Sql запрос
 * @param {array} params - Параметры запроса
 * @param {function} callback - Фукнция в которую приходит ответ
 */
function query(sql, params, callback) {
    let connect = mysql.createConnection(mysqlSettings);
    connect.connect();
    connect.query(sql, params,callback);
    connect.end();
}

module.exports = {
    query:query,
    mysqlSettings: mysqlSettings
};